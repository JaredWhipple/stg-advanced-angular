import { Component } from '@angular/core';

@Component({
  selector: 'stg-advanced-angular-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'stg-advanced-angular';
}
