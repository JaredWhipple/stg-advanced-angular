import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { StateService } from '../../services/state.service';
import { UserService } from '@stg-advanced-angular/lib/petstore-api';
import { HttpResponse } from '@angular/common/http';


@Component({
  selector: 'stg-advanced-angular-nav',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './nav.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavComponent {
  currentUser$ = this.state.currentUser$;

  constructor(private state: StateService, private userService: UserService) {}

  login() {
    const username = 'testuser';
    const password = 'password123';

    this.userService.loginUser(username, password, 'response')
      .subscribe({
        next: (response: HttpResponse<string>) => {
          // Extract the plain text response from the HttpResponse
          const textResponse = response.body;

          // Do something with the text response
          console.log(textResponse);
        },
        error: error => {
          console.log(error);
          // Handle the error here
        }
      });
  }

  logout() {
    this.userService.logoutUser();
  }
}
