import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeHtmlPipe } from '../../pipes/safe-html.pipe';

@Component({
  standalone: true,
  imports: [CommonModule, SafeHtmlPipe],
  templateUrl: './component-architecture.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentArchitectureComponent {
  codeExample1 = `<pre><code>
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent {
  @Input() title: string;
  @Input() body: string;
  @Input() author: string;
}
</code></pre>`
  codeExample2 = `<pre><code>
&lt;div class="post"&gt;
  &lt;h2&gt;{{ title }}&lt;/h2&gt;
  &lt;p&gt;{{ body }}&lt;/p&gt;
  &lt;p class="author"&gt;By {{ author }}&lt;/p&gt;
&lt;/div>
</code></pre>`

  codeExample3 = `<pre><code>
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  posts: any[] = [];

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getPosts().subscribe((data: any[]) => {
      this.posts = data;
    });
  }
}
</code></pre>`;

  codeExample4 = `<pre><code>
import { BlogComponent } from './blog/blog.component';
</code></pre>`;

  codeExample5 = `<pre><code>
const routes: Routes = [
  // ... other routes
  {
    path: 'blog',
    component: BlogComponent,
  },
];
</code></pre>`;


}
