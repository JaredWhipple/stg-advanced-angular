import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InstructionsComponent } from './instructions.component';

const routes: Routes = [
  { path: '', component: InstructionsComponent },
  { path: 'component-architecture', loadComponent: () => import('./component-architecture/component-architecture.component').then(c => c.ComponentArchitectureComponent)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstructionsRoutingModule { }
