import { Injectable } from '@angular/core';

import { PetService } from '@stg-advanced-angular/lib/petstore-api';
import { Pet } from '@stg-advanced-angular/lib/petstore-api';

import { BehaviorSubject, combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AngularModulesFacade {
  PetBS$ = new BehaviorSubject<Pet | null>(null);
  Pet$ = this.PetBS$.asObservable();

  vm$ = combineLatest({pet: this.Pet$})

  constructor(private petService: PetService) {}

  getPetById(petId: number) {
    this.petService.getPetById(petId).subscribe(res => this.PetBS$.next(res));
  }

}
