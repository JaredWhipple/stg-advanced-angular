import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularModulesRoutingModule } from './angular-modules-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AngularModulesComponent } from './angular-modules.component';



@NgModule({
  declarations: [AngularModulesComponent],
  imports: [
    CommonModule,
    AngularModulesRoutingModule,
    HttpClientModule
  ]
})
export class AngularModulesModule { }
