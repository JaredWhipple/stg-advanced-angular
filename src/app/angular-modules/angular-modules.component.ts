import { ChangeDetectionStrategy, Component } from '@angular/core';

import { AngularModulesFacade } from './angular-modules.facade';
import { StateService } from '../services/state.service';

@Component({
  templateUrl: './angular-modules.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AngularModulesComponent {

  vm$ = this.facade.vm$;

  constructor(private facade: AngularModulesFacade, private stateService: StateService) {
  }

  getPetById(petId: number): void {
    this.facade.getPetById(petId);
  }
}
