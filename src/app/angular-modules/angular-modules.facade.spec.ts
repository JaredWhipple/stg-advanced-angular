import { TestBed } from '@angular/core/testing';

import { AngularModulesFacade} from './angular-modules.facade';

describe('AngularModulesService', () => {
  let service: AngularModulesFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AngularModulesFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
