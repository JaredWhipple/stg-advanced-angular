import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularModulesComponent } from './angular-modules.component';

const routes: Routes = [{ path: '', component: AngularModulesComponent }];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [RouterModule]
})
export class AngularModulesRoutingModule { }