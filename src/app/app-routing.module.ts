import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  { path: '', loadComponent: () => import('./home/home.component').then(c => c.HomeComponent)},
  { path: 'angular-modules', loadChildren: () => import('./angular-modules/angular-modules.module').then(m => m.AngularModulesModule) },
  { path: 'instructions', loadChildren: () => import('./instructions/instructions.module').then(m => m.InstructionsModule) },
  { path: '**', loadComponent: () => import('./notFound/not-found.component').then(c => c.NotFoundComponent)}
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { initialNavigation: 'enabledBlocking' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }