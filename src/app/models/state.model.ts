import { User as CurrentUser } from 'oidc-client';

export class State {
  isLoading: boolean;
  currentUser: CurrentUser | null;

  constructor() {
    this.isLoading = false;
    this.currentUser = null;
  }
}
