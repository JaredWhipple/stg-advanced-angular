import { Injectable } from '@angular/core';
import { User as CurrentUser } from 'oidc-client';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, distinctUntilKeyChanged, map, scan } from 'rxjs/operators';

import { State } from '../models/state.model';

const initialState = new State();

@Injectable({
  providedIn: 'root',
})
export class StateService {
  protected stateSubject$: BehaviorSubject<State> = new BehaviorSubject<State>(initialState);
  state$: Observable<State> = this.stateSubject$.asObservable();

  private partialStateUpdate$: Subject<Partial<State>> = new Subject<Partial<State>>();

  get isLoading$(): Observable<boolean> {
    return this.stateSubject$.pipe(
      distinctUntilKeyChanged('isLoading'),
      map((state) => state.isLoading),
    );
  }

  get currentUser$(): Observable<CurrentUser | null> {
    return this.stateSubject$.pipe(
      distinctUntilKeyChanged('currentUser'),
      map((state) => state.currentUser),
    );
  }

  constructor() {
    this.partialStateUpdate$
      .pipe(scan((acc, curr) => ({ ...acc, ...curr }), initialState))
      .subscribe(this.stateSubject$);
  }

  patchState(partialState: Partial<State>): void {
    this.partialStateUpdate$.next(partialState);
  }

  selectKey(keyString: keyof State): Observable<State[keyof State]> {
    return this.stateSubject$.pipe(
      distinctUntilKeyChanged(keyString),
      map((key) => key[keyString]),
    );
  }

  selectKeys(keyArr: (keyof State)[]): Observable<Partial<State>> {
    return this.stateSubject$.pipe(
      distinctUntilChanged((prev, curr) => keyArr.some((key) => prev[key] !== curr[key])),
      map((val) => keyArr.reduce((acc, key: keyof State) => ({ ...acc, [key]: val[key] }), {})),
    );
  }
}